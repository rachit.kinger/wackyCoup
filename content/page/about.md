---
title: About
comments: false
---
My name is Rachit Kinger.  

I blog about things in data science, digital inequality and linux that I found difficult to understand or learn. Professionally, I am head of data and analytics at a media company in the UK called JPIMedia (erstwhile Johnston Press). We publish close to 200 news sites that cover mainly small towns and villages in the UK. Some of our big titles include [The Scotsman](https://www.scotsman.com), [The Yorkshire Post](https://www.yorkshirepost.co.uk), [inews](https://inews.co.uk) *(which won the British Media Awards for the website of the year - in the same year that Guardian launched its new website!)*.   

**Update:** one of my company's websites, [iNews](www.inews.co.uk) has recently been voted Britain's most trusted digital news brand - ahead of The Guardian and Times. Another, [The Scotsman](www.scotsman.com) is ranked number 5 in the same list. And yet another, [The Yorkshire Post](www.yorkshirepost.co.uk) is ranked number 1 in most trusted print newspapers - ahead of all the usual biggies like The Guardian, The Times, etc. Quite a feat! Don't believe me? Read about it [here](https://inews.co.uk/opinion/editor/trust-in-is-journalism-in-print-and-digital-is-at-record-high-according-to-new-industry-research/).   

### Talk to me about    

* Digital media - understanding user behaviour to inform content strategy, subscription strategy _(psst. especially subscription strategy)_  
* Building data pipelines in the cloud, especially Google Cloud  
* Text analytics / NLP / text mining   
* Data visualizations using Shiny, Data Studio, Plotly or simple pencil on paper :)  


### Lazy copy from [my twitter](https://twitter.com/rachitkinger) profile  
> One time editor, digital product guy, trained to navigate a ship but prefer cycling. Spend lots of time studying the internet. #datanerd #rstats #londonR    

A previous intro included my interest in music, especially bansuri, a traditional flute made of bamboo but I won't bore you with that here  
